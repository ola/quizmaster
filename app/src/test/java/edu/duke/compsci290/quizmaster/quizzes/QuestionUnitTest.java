package edu.duke.compsci290.quizmaster.quizzes;

import org.junit.Test;

//import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * Created by lpcox on 2/21/18.
 */

public class QuestionUnitTest {
    private Question mQuestion;
    private String mQuestionTitle;
    private WrongAnswer[] mWrongAnswers = {
            new WrongAnswer("Shane Battier"),
            new WrongAnswer("Bobby Hurley"),
            new WrongAnswer("Elton Brand")
    };
    private CorrectAnswer mCorrectAnswer;

    private void createQuestion () {
        mQuestionTitle = "Who is Duke's all-time leading scorer?";
        mQuestion = new Question(mQuestionTitle);
        mCorrectAnswer = new CorrectAnswer("J.J. Redick");

        // add the wrong answers to the question
        for (WrongAnswer w : mWrongAnswers) {
            mQuestion.addAnswer(w);
        }

        // add the correct answer to the question
        mQuestion.addCorrectAnswer(mCorrectAnswer);
    }

    // check that the question part of the Question is returned correctly
    @Test
    public void questionTitleIsCorrect() throws Exception {
        createQuestion ();
        assertEquals (mQuestionTitle, mQuestion.getQuestion());
    }

    // check that the wrong answers for the Question are returned correctly
    @Test
    public void answersAreCorrect() throws Exception {
        createQuestion ();
        // iterate through the answers to make sure they're all there
        boolean foundAnswer;
        for (IAnswer a : mQuestion.answerIterable()) {
            foundAnswer = false;
            for (WrongAnswer wrongAnswer : mWrongAnswers) {
                if (wrongAnswer.equals(a)) {
                    foundAnswer = true;
                    break;
                }
            }
            assert(foundAnswer);
        }
    }

    // check that the correct answers for the Question is returned correctly
    @Test
    public void correctAnswerIsCorrect() throws Exception {
        createQuestion ();
        assertEquals(mCorrectAnswer, mQuestion.correctAnswerIterable().iterator().next());
    }

    private void checkProcess(IAnswer a) {
        mQuestion.processChosen(a);
        Iterable<IAnswer> chosen = mQuestion.chosenIterable();
        boolean foundChosen = false;
        for (IAnswer c : chosen) {
            if (c.equals(a)) {
                foundChosen = true;
                break;
            }
        }
        assert(foundChosen);
    }

    // check that the Question processes incorrect answers correctly
    @Test
    public void processIncorrectIsCorrect() throws Exception {
        // give the question a wrong answer from the ones it has
        createQuestion ();
        checkProcess(mWrongAnswers[0]);

        // give the question a wrong answer it doesn't have
        createQuestion();
        mQuestion.processChosen(new WrongAnswer("Landon Cox"));
    }

    // check that the Question processes correct answers correctly
    @Test
    public void processCorrectIsCorrect() throws Exception {
        // give the question a correct answer using the ones it has
        createQuestion ();
        checkProcess(mCorrectAnswer);

        // give the question a copy of the correct answer
        createQuestion();
        mQuestion.processChosen(new CorrectAnswer(mCorrectAnswer.getAnswer()));

        // give the question a different correct answer than the one it has
        createQuestion();
        mQuestion.processChosen(new CorrectAnswer("Landon Cox"));
    }
}
