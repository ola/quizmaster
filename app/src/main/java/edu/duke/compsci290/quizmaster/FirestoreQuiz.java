package edu.duke.compsci290.quizmaster;

import java.util.List;

public class FirestoreQuiz {
    private String mId;
    private String mTitle;
    private String mGenre;
    private List<FirestoreQuestion> mQuestions;

    public FirestoreQuiz (){}

    // getters and setters
    public String getId() { return mId; }
    public void setId(String id) { mId = id; }
    public String getTitle(){
        return mTitle;
    }
    public void setTitle(String title) { mTitle = title; }
    public String getGenre(){
        return mGenre;
    }
    public void setGenre(String genre) { mGenre = genre; }
    public List<FirestoreQuestion> getQuestions() { return mQuestions; }
    public void setQuestions(List<FirestoreQuestion> qs) { mQuestions = qs; }
}
