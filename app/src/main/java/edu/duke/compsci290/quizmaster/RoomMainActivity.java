package edu.duke.compsci290.quizmaster;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.StringRequest;

import java.util.List;

public class RoomMainActivity extends AppCompatActivity {

    private Button mLeftButton;
    private Button myRightButton;
    private Quiz mQuiz;
    private TextView myQuestionView;
    private TextView mScoreView;
    private int mQuestionIndex;
    private int mCorrect;
    private String mScoreBase;
    private boolean mMenuInitialized = false;

    private static String sQUIZTITLE = "quiztitle";
    private static String INDEX = "INDEX";
    private static String SCORE = "SCORE";
    private static String REMOTE_QUIZ_TITLE = "Remote quiz";
    private static String REMOTE_QUIZ_URL = "https://users.cs.duke.edu/~lpcox/quiz.json";

    private static QuizDb mDatabase;

    private static final boolean USE_SHARED_PREFERENCES = false;
    private static final boolean USE_SQLITE = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        myQuestionView = (TextView) this.findViewById(R.id.question_text);
        mLeftButton = (Button) this.findViewById(R.id.left_button);
        myRightButton = (Button) this.findViewById(R.id.right_button);
        mScoreView = (TextView) this.findViewById(R.id.score_view);
        mScoreBase = mScoreView.getText().toString();

        if (savedInstanceState != null) {
            Toast.makeText(RoomMainActivity.this,
                    "create and saved != null",
                    Toast.LENGTH_SHORT).show();
        }
        Context c = getApplicationContext();
        XMLQuizGenerator.createQuizes(c);
        //roomResumeQuiz();

        new AsyncTask<Object, Object, String>() {
            @Override
            public String doInBackground(Object ... objs) {
                return roomResumeQuiz();
            }
            @Override
            public void onPostExecute(String title) {
                if (title != null) {
                    newGame(title);
                }
            }
        }.execute(new Object());

    }

    private String roomResumeQuiz() {
        if (mDatabase == null) {
            mDatabase = Room.databaseBuilder(getApplicationContext(),
                    QuizDb.class, "quiz").build();
        }

        List<QuizStatus> quizzes = mDatabase.quizDao().findUnfinishedQuizzes();

        if (!quizzes.isEmpty()) {
            return quizzes.get(0).getTitle();
        }
        return null;
    }

    private void roomAddQuiz() {
        if (mDatabase == null) {
            mDatabase = Room.databaseBuilder(getApplicationContext(),
                    QuizDb.class, "quiz").build();
        }

        // update the current quiz's timestamp
        long time = System.currentTimeMillis()/1000L;
        mQuiz.setTimestamp(time);

        QuizStatus q = mDatabase.quizDao().findByTitle(mQuiz.getTitle());

        // if the current quiz isn't already in the database, insert it
        if (q == null) {
            q = new QuizStatus();
            q.setTitle(mQuiz.getTitle());
            q.setTimestamp(mQuiz.getTimestamp());
            q.setFinished(0);
            q.setCorrect(0);
            q.setWrong(0);
            q.setLastQuestion(0);
            mDatabase.quizDao().insertAll(q);
        } else {
            // if the quiz is in the database, update the timestamp
            mDatabase.quizDao().updateTimestamp(mQuiz.getTitle(),
                    mQuiz.getTimestamp());
        }
    }

    private void roomStoreProgress() {
        if (mDatabase == null) {
            mDatabase = Room.databaseBuilder(getApplicationContext(),
                    QuizDb.class, "quiz").build();
        }

        // update the current quiz's timestamp
        long time = System.currentTimeMillis()/1000L;
        mQuiz.setTimestamp(time);

        QuizStatus q = mDatabase.quizDao().findByTitle(mQuiz.getTitle());
        if (q != null) {
            // if the quiz is in the database, update the timestamp
            mDatabase.quizDao().updateTimestamp(mQuiz.getTitle(),
                    mQuiz.getTimestamp());
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){

        mQuestionIndex = savedInstanceState.getInt(INDEX);
        mCorrect = savedInstanceState.getInt(SCORE);
        askQuestion();
        mQuestionIndex--;  // hack because of state in update
        updateScore();

    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt(INDEX, mQuestionIndex);
        state.putInt(SCORE, mCorrect);
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    public void leftClick(View button){
        Toast.makeText(RoomMainActivity.this,
                "that's correct! :-)",
                Toast.LENGTH_SHORT).show();
        mCorrect += 1;
        updateScore();
    }

    public void rightClick(View button){

        Toast.makeText(RoomMainActivity.this,
                "that's wrong :-(",
                Toast.LENGTH_SHORT).show();
        updateScore();
    }

    private void updateScore() {
        mQuestionIndex += 1;
        int remaining = mQuiz.size()-mQuestionIndex;
        String s = String.format(" %d/%d with %d to go",mCorrect, mQuiz.size(),remaining);
        mScoreView.setText(mScoreBase+s);
        if (mQuestionIndex < mQuiz.size()){
            askQuestion();
        }
        else {
            Toast.makeText(RoomMainActivity.this,
                    "quiz is over",
                    Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!mMenuInitialized) {
            String[] qtitles = XMLQuizGenerator.getQuizTitles();
            for (String title : qtitles) {
                menu.add(Menu.NONE, Menu.NONE, Menu.NONE, title);
                System.out.println("adding " + title);
            }
            // add a special option for a remote quiz
            menu.add(Menu.NONE, Menu.NONE, Menu.NONE, REMOTE_QUIZ_TITLE);
            mMenuInitialized = true;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("quizmaster", "MA.onOptionsItemSelected");

        String menuString = (String) item.getTitle();

        if (menuString.equals(REMOTE_QUIZ_TITLE)) {
            getRemoteQuiz();
        } else {

            newGame(menuString);

            (new AsyncTask<Object, Object, String>() {
                @Override
                public String doInBackground(Object... objs) {
                    roomAddQuiz();
                    return null;
                }

                @Override
                public void onPostExecute(String title) {

                }
            }).execute(new Object());
        }
        return super.onOptionsItemSelected(item);
    }

    private void newGame(String title) {
        Quiz q = XMLQuizGenerator.getQuiz(title);
        Log.d("quizmaster", "MA.newGame " + title);
        if (q != null) {
            Log.d("quizmaster", "MA.newGame returned " + q.toString());
            mQuiz = q;
            mQuestionIndex = 0;
            mCorrect = 0;
            askQuestion();
        } else {
            Log.d("quizmaster", "MA.newGame returned null");
            newGame();
        }
    }

    private void newGame() {
        mQuiz = XMLQuizGenerator.getQuiz(); //QuizGenerator.getQuiz();
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }
    private void newGame(Quiz q){
        mQuiz = q;
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }

    private void askQuestion() {
        OQuestion q = mQuiz.getQuestion(mQuestionIndex);
        myQuestionView.setText(q.getQuery());
        mLeftButton.setText(q.getCorrectAnswer());
        myRightButton.setText(q.getWrongAnswer());
    }

    private void getRemoteQuiz() {
        final RoomMainActivity a = this;
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET,
                REMOTE_QUIZ_URL,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        a.remoteQuizReady(response);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        a.remoteQuizReady("Failed to read JSON");
                    }
                });
        queue.add(request);
    }

    public void remoteQuizReady(String rawString){
        Toast.makeText(RoomMainActivity.this,
                "fetched "+rawString,
                Toast.LENGTH_LONG).show();

        Quiz q = JSONParser.parse(rawString);
        Log.d("json quiz", rawString);
        newGame(q);
    }

}
