package edu.duke.compsci290.quizmaster.TextQuizzer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.duke.compsci290.quizmaster.quizzes.IQuizzer;
import edu.duke.compsci290.quizmaster.quizzes.IQuizFactory;
import edu.duke.compsci290.quizmaster.quizzes.SimpleQuizFactory;

/**
 * Created by ola on 2/13/18.
 */

public class TextQuizzer {

    List<IQuizzer> mList;

    public TextQuizzer(IQuizFactory qf){
        mList = new ArrayList<>();
        for(IQuizzer q: qf.getQuizzes()) {
            mList.add(q);
        }
    }

    public String showTitles(){
        int count = 0;
        ArrayList<String> list = new ArrayList<>();
        for(IQuizzer q : mList) {
            list.add(q.getTitle());
            System.out.printf("%d)\t%s\n",count,q.getTitle());
            count += 1;
        }
        Scanner in = new Scanner(System.in);
        System.out.print("quiz> ");
        int response = in.nextInt();
        return list.get(response);
    }

    public void giveQuiz(String title){
        for(IQuizzer q : mList) {
            if (q.getTitle().equals(title)) {
                QuizGiver.giveQuiz(q);
            }
        }
    }

    public static void main(String[] args) {
        TextQuizzer tq = new TextQuizzer(SimpleQuizFactory.getInstance());
        String s = tq.showTitles();
        tq.giveQuiz(s);

    }
}
