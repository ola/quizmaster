package edu.duke.compsci290.quizmaster.quizzes;

import org.json.JSONException;

/**
 * Created by ola on 2/13/18.
 */

public interface IQuizzer {
     String getTitle();
     Iterable<Question> getQuestions();
     String processResults() throws QuizResultException;
}
