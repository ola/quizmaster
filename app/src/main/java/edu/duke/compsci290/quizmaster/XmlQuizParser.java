package edu.duke.compsci290.quizmaster;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lpcox on 2/9/17.
 */

public class XmlQuizParser {
    // We don't use namespaces
    private static final String ns = null;

    public Quiz parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readQuiz(parser);
        } finally {
            in.close();
        }
    }

    private Quiz readQuiz(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<OQuestion> questionList = new ArrayList<OQuestion>();
        String title = "";
        parser.require(XmlPullParser.START_TAG, ns, "quiz");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();

            if (name.equals("title")) {
                title = readTitle(parser);
            } else if (name.equals("question")){
                questionList.add(readQuestion(parser));
            } else {
                skip(parser);
            }
        }
        OQuestion[] questions = new OQuestion[questionList.size()];
        questionList.toArray(questions);
        return new Quiz(title, questions);
    }

    private OQuestion readQuestion(XmlPullParser parser) throws IOException, XmlPullParserException {
        String q = "";
        String right = "";
        List<String> wronglist = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "question");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();

            if (name.equals("text")) {
                q = readText(parser);
            } else if (name.equals("correctanswer")){
                right = readText(parser);
            } else if (name.equals("wronganswer")) {
                wronglist.add(readText(parser));
            } else {
                skip(parser);
            }
        }

        String[] wrongs = new String[wronglist.size()];
        wronglist.toArray(wrongs);
        return new OQuestion(q, right, wrongs);
    }

    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "title");
        return title;
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
