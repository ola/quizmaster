package edu.duke.compsci290.quizmaster;

import android.util.Log;

/**
 * Created by ola on 2/6/17.
 */

public class OQuestion {
    private String mQuery;
    private String mCorrect;
    private String[] mWrongAnswers;

    public OQuestion(String q, String right, String wrong) {

        mQuery = q;
        mCorrect = right;
        mWrongAnswers = new String[]{wrong};
    }
    public OQuestion(String q, String right, String[] wrongs) {
        mQuery = q;
        mCorrect = right;
        mWrongAnswers = new String[wrongs.length];
        System.arraycopy(wrongs,0,mWrongAnswers,0,wrongs.length);
    }
    public String getQuery() {
        return mQuery;
    }

    public String getCorrectAnswer() {
        return mCorrect;
    }

    public String getWrongAnswer() {
        return mWrongAnswers[0];
    }

    public String[] getWrongAnswers() {
        return mWrongAnswers;
    }
    public int responseCount(){
        return mWrongAnswers.length + 1;
    }

    private boolean answersSame(String[] answers) {
        for (String a:answers) {
            boolean found = false;
            for (String myA: mWrongAnswers) {
                if (a.equals(myA)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                Log.d("OQuestion","couldn't find " + a);
                return false;
            }
        }
        return true;
    }

    public boolean equals (Object o) {
        if (! (o instanceof OQuestion)) {
            return false;
        }
        OQuestion q = (OQuestion) o;
        return q.getCorrectAnswer().equals(mCorrect) &&
                q.getQuery().equals(mQuery) &&
                answersSame(q.getWrongAnswers());
    }
}
