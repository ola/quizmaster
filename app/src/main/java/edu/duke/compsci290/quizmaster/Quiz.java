package edu.duke.compsci290.quizmaster;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by ola on 2/6/17.
 */

@Entity
public class Quiz {
    private ArrayList<OQuestion> myQuestions;

    @PrimaryKey
    private int mID;

    @ColumnInfo(name = "quiz_title")
    private String mTitle;

    @ColumnInfo(name = "num_correct")
    private int mCorrect;

    @ColumnInfo(name = "num_wrong")
    private int mWrong;

    @ColumnInfo(name = "last_question")
    private int mLastQuestion;

    @ColumnInfo(name = "finished_quiz")
    private int mFinished;

    @ColumnInfo(name = "timestamp")
    private long mTimestamp;

    public Quiz() {}
    public Quiz(FirestoreQuiz fsQ) {
        mTitle = fsQ.getTitle();
        myQuestions = new ArrayList<OQuestion>();
        for(FirestoreQuestion q : fsQ.getQuestions()) {
            String query = q.getQuestion();
            String correct = q.getCorrect();
            String wrong = q.getIncorrect().keySet().iterator().next();
            OQuestion oq = new OQuestion(query, correct, wrong);
            myQuestions.add(oq);
        }
    }
    public Quiz(String title, OQuestion[] questions) {
        this(title,questions,false);
    }
    public Quiz(String title, OQuestion[] questions, boolean shouldShuffle) {
        mTitle = title;
        myQuestions = new ArrayList<>(Arrays.asList(questions));
        if (shouldShuffle){
            Collections.shuffle(myQuestions);
        }
    }

    public int size(){
        return myQuestions.size();
    }

    public OQuestion getQuestion(int index){
        if (0 <= index && index < myQuestions.size()){
            return myQuestions.get(index);
        }
        throw new IndexOutOfBoundsException("bad index "+index);
    }

    // Room getters and setters
    public int getID() { return mID; }
    public void setID(int id) { mID = id; }
    public String getTitle(){
        return mTitle;
    }
    public void setTitle(String title) { mTitle = title; }
    public int getNumCorrect() { return mCorrect; }
    public void setNumCorrect(int num) { mCorrect = num; }
    public int getNumWrong() { return mWrong; }
    public void setNumWrong(int num) { mWrong = num; }
    public int getLastQuestion() { return mLastQuestion; }
    public void setLastQuestion(int last) { mLastQuestion = last; }
    public int getFinishedQuiz() { return mFinished; }
    public void setFinishedQuiz(int finished) { mFinished = finished; }
    public long getTimestamp() { return mTimestamp; }
    public void setTimestamp(long timestamp) { mTimestamp = timestamp; }
}
