package edu.duke.compsci290.quizmaster.quizzes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ola on 2/13/18.
 */

public class LinearQuiz implements IPackageQuizzer {


    String mTitle;
    private List<Question> mList;

    @Override
    public void setTitle(String source) {
        mTitle = source;
        mList = new ArrayList<>();
    }

    @Override
    public void addQuestion(Question question) {
        mList.add(question);
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public Iterable<Question> getQuestions() {
        return mList;
    }

    @Override
    public String processResults() throws QuizResultException  {
        try {
            JSONObject jo = new JSONObject();
            jo.put("title", getTitle());
            for (Question q : mList) {
                JSONObject qo = new JSONObject();
                qo.put("question", q.getQuestion());
                Iterable<IAnswer> correctAnswers = q.correctAnswerIterable();
                Iterable<IAnswer> chosen = q.chosenIterable();

                // for each chosen answer add whether it was correct to the question object
                for (IAnswer c : chosen) {
                    boolean correct = false;
                    qo.put("answer", c.getAnswer());
                    // check whether the answer was correct (assumes you only need one correct answer)
                    for (IAnswer a : correctAnswers) {
                        if (a.equals(c)) {
                            correct = true;
                            break;
                        }
                    }
                    qo.put("correct", correct);
                }

                // put the question object in the root json object
                jo.put("question", qo);

            }
            return jo.toString();
        } catch (JSONException e) {
            throw new QuizResultException("oh no JSON");
        }
    }
}
