package edu.duke.compsci290.quizmaster;

import android.provider.BaseColumns;

/**
 * Created by lpcox on 3/1/17.
 */

public final class QuizmasterContract {
    private QuizmasterContract() {}

    public static class QuizProgress implements BaseColumns {
        public static final String TABLE_NAME = "quizprogress";
        public static final String COLUMN_NAME_TITLE = "quiz_title";
        public static final String COLUMN_NAME_NUMCORRECT = "num_correct";
        public static final String COLUMN_NAME_NUMWRONG = "num_wrong";
        public static final String COLUMN_NAME_LASTQUESTION = "last_question";
        public static final String COLUMN_NAME_FINISHED = "finished_quiz";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

    }

    // user profile table here?
    public static class UserEntry implements BaseColumns {

    }
}
