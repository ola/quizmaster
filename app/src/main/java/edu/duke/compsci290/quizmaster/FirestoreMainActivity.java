package edu.duke.compsci290.quizmaster;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirestoreMainActivity extends AppCompatActivity {

    private Button mLeftButton;
    private Button myRightButton;
    private Quiz mQuiz;
    private TextView myQuestionView;
    private TextView mScoreView;
    private int mQuestionIndex;
    private int mCorrect;
    private String mScoreBase;
    private boolean mMenuInitialized = false;

    private static FirestoreMainActivity SINGLETON;
    private static String INDEX = "INDEX";
    private static String SCORE = "SCORE";
    private static String REMOTE_QUIZ_TITLE = "Loading quizzes";
    private static String TAG = "FirestoreMainActivity";

    private FirebaseAuth mAuth;
    private static FirebaseFirestore mFirestore;
    private static final int RC_SIGN_IN = 123;

    private static Map<String, FirestoreQuiz> mQuizzes;
    private static QuizDb mDatabase;

    private static final boolean USE_SHARED_PREFERENCES = false;
    private static final boolean USE_SQLITE = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        SINGLETON = this;
        myQuestionView = (TextView) this.findViewById(R.id.question_text);
        mLeftButton = (Button) this.findViewById(R.id.left_button);
        myRightButton = (Button) this.findViewById(R.id.right_button);
        mScoreView = (TextView) this.findViewById(R.id.score_view);
        mScoreBase = mScoreView.getText().toString();

        if (savedInstanceState != null) {
            Toast.makeText(FirestoreMainActivity.this,
                    "create and saved != null",
                    Toast.LENGTH_SHORT).show();
        }
        mQuizzes = new HashMap<String, FirestoreQuiz>();
        mFirestore = FirebaseFirestore.getInstance();

        // load the remote quizzes
        fsLoadQuizzes();

        mAuth = FirebaseAuth.getInstance();
        firebaseCheckUser();
    }

    private void firebaseCheckUser() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            launchSignIn();
        } else {
            String oldText = myQuestionView.getText().toString();
            myQuestionView.setText("Welcome " + user.getDisplayName() + "!" +
                    "\n\n" + oldText + "\n");
            Log.d(TAG, "Successful login: " + user.getUid() +
                    " (" +  user.getDisplayName() + ")");
            //fsResumeQuiz();
        }
    }

    private void launchSignIn() {

        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Log.w(TAG, "Successful login: " + user.getUid() +
                " (" +  user.getDisplayName() + ")");

                myQuestionView.setText("Welcome " + user.getDisplayName() + "!");

                // now add the device token to the users database
                String empty = "";
                SharedPreferences s = getSharedPreferences(this.getString(R.string.fcm_token_file),
                        Context.MODE_PRIVATE);
                String token = s.getString(this.getString(R.string.fcm_token), empty);
                FirestoreUser fu = new FirestoreUser();
                fu.setUid(user.getUid());
                if (!token.equals(empty)) {
                    HashMap<String, Boolean> map = new HashMap<String, Boolean>();
                    map.put(token, true);
                    fu.setDevices(map);
                }

                mFirestore.collection("users")
                        .add(fu)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });
            } else {
                // try again
                Log.w(TAG, "Login failed: " + resultCode);
                launchSignIn();
            }
        }
    }

    private void fsLoadQuizzes() {
        mFirestore.collection("quizzes")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                FirestoreQuiz q = document.toObject(FirestoreQuiz.class);
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                mQuizzes.put(q.getTitle(), q);
                            }
                            // now load the quizzes into the menu
                            invalidateOptionsMenu();
                        } else {
                            Log.w(TAG, "No quizzes.", task.getException());
                        }
                    }
                });
    }

    private void fsResumeQuiz() {
        FirebaseUser user = mAuth.getCurrentUser();
        mFirestore.collection("quiz_statuses")
                .whereEqualTo("uid", user.getUid())
                .whereEqualTo("finished", false) // look for unfinished quizzes
                .orderBy("timestamp") // order results by timestamp
                .limit(1) // only get the most recently taken quiz
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            // get a reference to the quiz status
                            DocumentSnapshot doc = task.getResult().getDocuments().get(0);
                            // load the quiz w/ the id of the most recently taken, unfinished quiz
                            fsAddQuiz(doc.getString("quizid"));
                            FirestoreQuizStatus status = doc.toObject(FirestoreQuizStatus.class);
                            Log.d(TAG, status.getQuizid() + " => " + doc.getData());
                        } else {
                            Log.w(TAG, "No quiz statuses.", task.getException());
                        }
                    }
                });
    }

    private void fsAddQuiz(String quizID) {
        mFirestore.collection("quizzes")
                .document(quizID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot> () {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            // get a reference to the quiz
                            DocumentSnapshot doc = task.getResult();
                            Log.d(TAG, doc.getId() + " => " + doc.getData());
                        } else {
                            Log.w(TAG, "No quizzes.", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){
        mQuestionIndex = savedInstanceState.getInt(INDEX);
        mCorrect = savedInstanceState.getInt(SCORE);
        askQuestion();
        mQuestionIndex--;  // hack because of state in update
        updateScore();
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt(INDEX, mQuestionIndex);
        state.putInt(SCORE, mCorrect);
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    public void leftClick(View button){
        Toast.makeText(FirestoreMainActivity.this,
                "that's correct! :-)",
                Toast.LENGTH_SHORT).show();
        mCorrect += 1;
        updateScore();
    }

    public void rightClick(View button){

        Toast.makeText(FirestoreMainActivity.this,
                "that's wrong :-(",
                Toast.LENGTH_SHORT).show();
        updateScore();
    }

    private void updateScore() {
        mQuestionIndex += 1;
        int remaining = mQuiz.size()-mQuestionIndex;
        String s = String.format(" %d/%d with %d to go",mCorrect, mQuiz.size(),remaining);
        mScoreView.setText(mScoreBase+s);
        if (mQuestionIndex < mQuiz.size()){
            askQuestion();
        }
        else {
            Toast.makeText(FirestoreMainActivity.this,
                    "quiz is over",
                    Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!mMenuInitialized) {
            mMenuInitialized = true;
            menu.add(Menu.NONE, Menu.NONE, Menu.NONE, REMOTE_QUIZ_TITLE);
        }
        if (!mQuizzes.isEmpty()) {
            // clear whatever was there before
            menu.clear();

            // load the remote quizzes
            for (String title : mQuizzes.keySet()) {
                menu.add(Menu.NONE, Menu.NONE, Menu.NONE, title);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("quizmaster", "MA.onOptionsItemSelected");
        String menuString = (String) item.getTitle();
        FirestoreQuiz fsQ = mQuizzes.get(menuString);
        if (fsQ != null) {
            Quiz q = new Quiz (fsQ);
            newGame(q);
        }
        return super.onOptionsItemSelected(item);
    }

    private void newGame(Quiz q){
        mQuiz = q;
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }

    private void askQuestion() {
        OQuestion q = mQuiz.getQuestion(mQuestionIndex);
        myQuestionView.setText(q.getQuery());
        mLeftButton.setText(q.getCorrectAnswer());
        myRightButton.setText(q.getWrongAnswer());
    }

    public static FirestoreMainActivity getActivity() {
        return SINGLETON;
    }
}
