package edu.duke.compsci290.quizmaster.quizzes;

import java.util.Iterator;
import java.util.List;

/**
 * Created by ola on 2/20/18.
 */

public class NonlinearQuiz implements IPackageQuizzer {

    private List<Question> mEasyQuestions;
    private List<Question> mHardQuestions;

    @Override
    public void setTitle(String source) {

    }

    @Override
    public void addQuestion(Question question) {
        mEasyQuestions.add(question);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public Iterable<Question> getQuestions() {
        return new QuestionIterable();
    }

    @Override
    public String processResults() throws QuizResultException {
        return null;
    }

    private class QuestionIterable implements Iterable<Question> {
        @Override
        public Iterator<Question> iterator() {
            return new QuestionIterator();
        }

    }

    private class QuestionIterator implements Iterator<Question> {
        @Override
        public boolean hasNext() {

            if (mEasyQuestions.isEmpty()) {
                return false;
            }
            return true;
        }

        @Override
        public Question next() {
            Question q = mEasyQuestions.get(0);
            mEasyQuestions.remove(0);
            return q;
        }
    }
}
