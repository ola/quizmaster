package edu.duke.compsci290.quizmaster;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class QuizmasterFirebaseMessagingService extends FirebaseMessagingService {

    private static String TAG = "QuizmasterFirebaseMessagingService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        Handler handler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg){
                String mymsg = (String) msg.obj;
                Toast.makeText(FirestoreMainActivity.getActivity(),
                        mymsg,
                        Toast.LENGTH_SHORT).show();
            }
        };

        Message m = Message.obtain(handler, 0,
                remoteMessage.getNotification().getBody());
        m.sendToTarget();

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }}
