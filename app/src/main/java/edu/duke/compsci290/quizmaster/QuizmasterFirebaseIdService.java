package edu.duke.compsci290.quizmaster;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class QuizmasterFirebaseIdService extends FirebaseInstanceIdService {
    private static String TAG = "QUIZMASTERFIREBASESERVICE";
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // store the token in shared preferences
        SharedPreferences preferences = getSharedPreferences(this.getString(R.string.fcm_token_file),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = preferences.edit();
        ed.putString(this.getString(R.string.fcm_token),
                refreshedToken);
        ed.commit();
    }
}
