package edu.duke.compsci290.quizmaster;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import edu.duke.compsci290.quizmaster.QuizmasterContract.QuizProgress;


/**
 * Created by lpcox on 3/1/17.
 */

public class QuizmasterDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "quizmaster.db";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " +
                    QuizProgress.TABLE_NAME + " (" +
                    QuizProgress._ID + " INTEGER PRIMARY KEY," +
                    QuizProgress.COLUMN_NAME_TITLE + " TEXT," +
                    QuizProgress.COLUMN_NAME_NUMCORRECT + " INT," +
                    QuizProgress.COLUMN_NAME_NUMWRONG + " INT," +
                    QuizProgress.COLUMN_NAME_LASTQUESTION + " INT," +
                    QuizProgress.COLUMN_NAME_FINISHED + " INT," +
                    QuizProgress.COLUMN_NAME_TIMESTAMP + " INT)";

    private static final String SQL_DELETE_TABLE =
            "DROP TABLE IF EXISTS " + QuizProgress.TABLE_NAME;

    public QuizmasterDbHelper(Context c) {
        // don't pass in a SQLiteCursorFactory
        super (c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }
}
