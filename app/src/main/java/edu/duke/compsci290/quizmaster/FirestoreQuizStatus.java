package edu.duke.compsci290.quizmaster;

import java.util.Date;

public class FirestoreQuizStatus {
    private String mQuizID;
    private String mUID;
    private String mTitle;
    private int mCorrect;
    private int mWrong;
    private int mLastQuestion;
    private boolean mFinished;
    private Date mTimestamp;

    public FirestoreQuizStatus() {}

    // getters and setters
    public String getUID() { return mUID; }
    public void setUID(String id) { mUID = id; }
    public String getQuizid() { return mQuizID; }
    public void setQuizid(String id) { mQuizID = id; }
    public String getTitle(){
        return mTitle;
    }
    public void setTitle(String title) { mTitle = title; }
    public int getCorrect() { return mCorrect; }
    public void setCorrect(int num) { mCorrect = num; }
    public int getWrong() { return mWrong; }
    public void setWrong(int num) { mWrong = num; }
    public int getLastquestion() { return mLastQuestion; }
    public void setLastquestion(int last) { mLastQuestion = last; }
    public boolean getFinished() { return mFinished; }
    public void setFinished(boolean finished) { mFinished = finished; }
    public Date getTimestamp() { return mTimestamp; }
    public void setTimestamp(Date timestamp) { mTimestamp = timestamp; }
}
