package edu.duke.compsci290.quizmaster.quizzes;

/**
 * Created by ola on 2/13/18.
 */

public interface IQuizFactory {
    Iterable<IQuizzer> getQuizzes();
}
