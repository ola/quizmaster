package edu.duke.compsci290.quizmaster.quizzes;

/**
 * Created by ola on 2/13/18.
 */

public class WrongAnswer implements IAnswer {
    private String mSource;
    private boolean mChosen;

    WrongAnswer(String s){
        create(s);
    }

    @Override
    public void create(String source) {
        mSource = source;
        mChosen = false;
    }

    @Override
    public String getAnswer() {
        return mSource;
    }

    @Override
    public void setChosen() {
        mChosen = true;
    }

    @Override
    public int hashCode(){
        return mSource.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (! (o instanceof WrongAnswer)) {
            return false;
        }
        WrongAnswer ca = (WrongAnswer) o;
        return getAnswer().equals(ca.getAnswer());
    }
}
