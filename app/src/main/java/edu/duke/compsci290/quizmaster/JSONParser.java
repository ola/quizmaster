package edu.duke.compsci290.quizmaster;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ola on 2/14/17.
 */

public class JSONParser {
    public static Quiz parse(String jString){
        try {
            JSONObject all = new JSONObject(jString);
            String qtitle = all.getString("title");
            JSONArray array = all.getJSONArray("questions");
            OQuestion[] questions = new OQuestion[array.length()];
            for(int k=0; k < array.length(); k++){
                JSONObject current = array.getJSONObject(k);
                String year = current.getString("question");
                String correct = current.getString("correct");
                JSONArray wrongs = current.getJSONArray("incorrect");
                String[] wrongAnswers = new String[wrongs.length()];
                for(int j=0; j < wrongAnswers.length; j++){
                    wrongAnswers[j] = wrongs.getString(j);
                }
                OQuestion q = new OQuestion("Best picture in "+year, correct, wrongAnswers);
                questions[k] = q;
            }
            Quiz qz = new Quiz(qtitle,questions);
            return qz;
        } catch (JSONException e) {
            Log.d("json parse","error in parsing");
            e.printStackTrace();
        }
        return null;
    }
}
