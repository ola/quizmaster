package edu.duke.compsci290.quizmaster.quizzes;

/**
 * Created by ola on 2/13/18.
 */

public interface IAnswer {
    void create(String source);
    public String getAnswer();
    public void setChosen();
}
