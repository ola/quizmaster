package edu.duke.compsci290.quizmaster.TextQuizzer;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Scanner;

import edu.duke.compsci290.quizmaster.quizzes.IAnswer;
import edu.duke.compsci290.quizmaster.quizzes.IQuizzer;
import edu.duke.compsci290.quizmaster.quizzes.Question;
import edu.duke.compsci290.quizmaster.quizzes.QuizResultException;

/**
 * Created by ola on 2/13/18.
 */

class QuizGiver {
    static Scanner in;
    static {
        in = new Scanner(System.in);
    }
    public static void giveQuiz(IQuizzer q) {
        System.out.println(q.getTitle());


        for(Question quest : q.getQuestions()) {
            ArrayList<IAnswer> ans = new ArrayList<>();
            ans.add(null);  // user uses 1,2,3 not 0,1,2

            System.out.println(quest.getQuestion());
            int count = 1;

            for(IAnswer answer : quest.answerIterable()) {
                ans.add(answer);
                System.out.printf("%d)\t%s\n",count,answer.getAnswer());
                count += 1;
            }
            System.out.println("---------");
            System.out.printf("your answer> ");
            int response = in.nextInt();
            quest.processChosen(ans.get(response));

        }

        System.out.println("all finished -------");
        try {
            String s = q.processResults();
            System.out.println(s);
        }
        catch(QuizResultException je) {
            throw new RuntimeException("json broken");
        }
    }
}
