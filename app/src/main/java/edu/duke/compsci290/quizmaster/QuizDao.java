package edu.duke.compsci290.quizmaster;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by lpcox on 3/27/18.
 */
@Dao
public interface QuizDao {
    @Query("SELECT * FROM quizstatus")
    List<QuizStatus> getAll();

    @Query("SELECT * FROM quizstatus WHERE id IN (:quizIds)")
    List<QuizStatus> loadAllByIds(int[] quizIds);

    @Query("SELECT * FROM quizstatus WHERE quiz_title LIKE :title LIMIT 1")
    QuizStatus findByTitle(String title);

    @Query("SELECT * FROM quizstatus WHERE finished_quiz=0 ORDER BY timestamp DESC")
    List<QuizStatus> findUnfinishedQuizzes();

    @Query("UPDATE quizstatus SET timestamp=:timestamp WHERE quiz_title LIKE :title")
    void updateTimestamp(String title, long timestamp);

    @Insert
    void insertAll(QuizStatus... quizzes);

    @Delete
    void delete(QuizStatus quiz);
}
