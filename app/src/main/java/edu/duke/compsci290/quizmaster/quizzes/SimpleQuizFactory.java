package edu.duke.compsci290.quizmaster.quizzes;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by ola on 2/13/18.
 */

public class SimpleQuizFactory implements IQuizFactory {

    private static SimpleQuizFactory mInstance;
    private List<IQuizzer> mQuizList;

    public static SimpleQuizFactory getInstance(){
        if (mInstance == null) {
           mInstance = new SimpleQuizFactory();
        }
        return mInstance;
    }

    private void doConstruct() {
        mQuizList = new ArrayList<>();
        LinearQuiz qz = new LinearQuiz();
        qz.setTitle("World Capitals");
        Question q1 = new Question("What is the Capital of France");
        q1.addAnswer(new CorrectAnswer("Paris"));
        q1.addAnswer(new WrongAnswer("Berlin"));
        q1.addAnswer(new WrongAnswer("Marseilles"));
        qz.addQuestion(q1);

        q1 = new Question("What is the Capital of Indonesia");
        q1.addAnswer(new CorrectAnswer("Jarkata"));
        q1.addAnswer(new WrongAnswer("Surabaya"));
        q1.addAnswer(new WrongAnswer("Bandung"));
        qz.addQuestion(q1);

        q1 = new Question("What is the Capital of South Africa");
        q1.addAnswer(new CorrectAnswer("Cape Town"));
        q1.addAnswer(new CorrectAnswer("Pretoria"));
        q1.addAnswer(new WrongAnswer("Johannesburg"));
        q1.addAnswer(new WrongAnswer("Durban"));
        qz.addQuestion(q1);

        mQuizList.add(qz);
        mQuizList.add(getStateBirdQuiz());

    }

    private LinearQuiz getStateBirdQuiz(){
        LinearQuiz qz = new LinearQuiz();
        qz.setTitle("State Birds");
        Question q1 = new Question("What is the state bird of North Carolina");
        q1.addAnswer(new CorrectAnswer("cardinal"));
        q1.addAnswer(new WrongAnswer("woodpecker"));
        q1.addAnswer(new WrongAnswer("blue jay"));
        qz.addQuestion(q1);

        q1 = new Question("What is the state bird of Oklahoma");
        q1.addAnswer(new CorrectAnswer("Scissor-tailed Flycatcher"));
        q1.addAnswer(new CorrectAnswer("Wild Turkey"));
        q1.addAnswer(new WrongAnswer("Ruffled Grouse"));
        qz.addQuestion(q1);

        return qz;

    }

    public Iterable<IQuizzer> getQuizzes(){
        return mQuizList;
    }

    private SimpleQuizFactory() {
        doConstruct();
    }
}
