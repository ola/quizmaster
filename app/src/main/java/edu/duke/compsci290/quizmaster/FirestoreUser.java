package edu.duke.compsci290.quizmaster;

import java.util.List;
import java.util.Map;

public class FirestoreUser {
    private String mUID;
    private Map<String, Boolean> mDevices;

    public FirestoreUser (){}

    // getters and setters
    public String getUid() { return mUID; }
    public void setUid(String id) { mUID = id; }
    public Map<String, Boolean> getDevices() { return mDevices; }
    public void setDevices(Map<String, Boolean> m) { mDevices = m; }
}
