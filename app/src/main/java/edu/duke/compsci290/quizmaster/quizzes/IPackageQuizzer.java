package edu.duke.compsci290.quizmaster.quizzes;

import org.json.JSONException;

/**
 * Created by ola on 2/13/18.
 */

interface IPackageQuizzer extends IQuizzer {
    void setTitle(String source);
    void addQuestion(Question question);

    // section below is redundant, not needed

    String getTitle();
    Iterable<Question> getQuestions();
    String processResults() throws QuizResultException;

}
