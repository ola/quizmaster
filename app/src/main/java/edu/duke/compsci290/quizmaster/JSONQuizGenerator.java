package edu.duke.compsci290.quizmaster;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ola on 2/13/17.
 */

public class JSONQuizGenerator {
    private int mQuizID;
    private static String sTAG = "JSONCODE";

    public JSONQuizGenerator(int quizID) throws MalformedURLException {
        mQuizID = quizID;
    }

    public String getJSON(final Context c) {
        return c.getResources().getString(mQuizID);
    }

    public String getJSON(final MainActivity quizMain){
        Context c = quizMain.getApplicationContext();
        return c.getResources().getString(mQuizID);
    }

}
