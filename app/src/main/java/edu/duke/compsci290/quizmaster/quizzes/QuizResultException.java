package edu.duke.compsci290.quizmaster.quizzes;

/**
 * Created by ola on 2/13/18.
 */

public class QuizResultException extends Exception {
    private final String mMessage;

    public QuizResultException(String message){
        mMessage = message;
    }

    @Override
    public String toString(){
        return mMessage;
    }
}
