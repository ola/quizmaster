package edu.duke.compsci290.quizmaster;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by ola on 1/26/17.
 */

public class QuizGenerator {


    private static Map<String,Quiz> ourQuizzes;
    private static final String sDEFAULT = "miscellaneous";

    static {
        ourQuizzes = new HashMap<>();
        createMiscQuiz();
        createSongQuiz();
    }

    private static void createMiscQuiz() {
        OQuestion[] q = {
                new OQuestion("capital of ecuador","quito","ecuador city"),
                new OQuestion("what day is it?", "today", "tomorrow"),
                new OQuestion("number of ergs in a joule","10^-7","10^7"),
                new OQuestion("who created Python","guido","niklaus"),
        };
        Quiz q2 = new Quiz(sDEFAULT,q);
        ourQuizzes.put(q2.getTitle(),q2);
    }

    private static void createSongQuiz(){

        OQuestion[] q = {
                new OQuestion("Thrift Shop top song in","2013",
                        new String[]{"2011", "2012", "2014"}),
                new OQuestion("Gotye had top song in","2012",
                        new String[]{"2010", "2011", "2013"}),
                new OQuestion("Call Me Maybe is by","Carly Rae Jepson",
                        new String[]{"Adele","Katy Perry","Ellie Goulding"}),
                new OQuestion("Who had two songs in top 5 across 2014-2016","Justin Bieber",
                        new String[]{"Maroon 5","Ed Sheeran","Rihanna"}),
        };
        Quiz q2 = new Quiz("songs and years",q);
        ourQuizzes.put(q2.getTitle(),q2);
    }

    public static Quiz getQuiz(String s) {
        if (ourQuizzes.containsKey(s)) {
            return ourQuizzes.get(s);
        }
        for(String rr : ourQuizzes.keySet()){
            return ourQuizzes.get(rr);
        }
        // never reached if anything in map
        return null;
    }

    public static Quiz getQuiz(){
        return getQuiz(sDEFAULT);
    }

    public static String[] getQuizTitles(){
        Set<String> titles = ourQuizzes.keySet();
        return titles.toArray(new String[0]);
    }

    private QuizGenerator(){
        // nada
    }
}
