package edu.duke.compsci290.quizmaster;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by lpcox on 3/20/18.
 */

@Database(entities = {QuizStatus.class}, version = 1)
public abstract class QuizDb extends RoomDatabase {
    public abstract QuizDao quizDao();
}