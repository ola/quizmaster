package edu.duke.compsci290.quizmaster;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by ola on 2/6/17.
 */

@Entity
public class QuizStatus {

    @PrimaryKey
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "quiz_title")
    private String mTitle;

    @ColumnInfo(name = "num_correct")
    private int mCorrect;

    @ColumnInfo(name = "num_wrong")
    private int mWrong;

    @ColumnInfo(name = "last_question")
    private int mLastQuestion;

    @ColumnInfo(name = "finished_quiz")
    private int mFinished;

    @ColumnInfo(name = "timestamp")
    private long mTimestamp;

    //public QuizStatus() {}

    // Room getters and setters
    public int getId() { return mId; }
    public void setId(int i) { mId = i; }
    public String getTitle(){
        return mTitle;
    }
    public void setTitle(String title) { mTitle = title; }
    public int getCorrect() { return mCorrect; }
    public void setCorrect(int num) { mCorrect = num; }
    public int getWrong() { return mWrong; }
    public void setWrong(int num) { mWrong = num; }
    public int getLastQuestion() { return mLastQuestion; }
    public void setLastQuestion(int last) { mLastQuestion = last; }
    public int getFinished() { return mFinished; }
    public void setFinished(int finished) { mFinished = finished; }
    public long getTimestamp() { return mTimestamp; }
    public void setTimestamp(long timestamp) { mTimestamp = timestamp; }
}
