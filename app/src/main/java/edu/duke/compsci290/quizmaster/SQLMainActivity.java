package edu.duke.compsci290.quizmaster;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import edu.duke.compsci290.quizmaster.QuizmasterContract.QuizProgress;

public class SQLMainActivity extends AppCompatActivity {

    private Button mLeftButton;
    private Button myRightButton;
    private Quiz mQuiz;
    private TextView myQuestionView;
    private TextView mScoreView;
    private int mQuestionIndex;
    private int mCorrect;
    private String mScoreBase;
    private boolean mMenuInitialized = false;

    private static String sQUIZTITLE = "quiztitle";
    private static String INDEX = "INDEX";
    private static String SCORE = "SCORE";

    private static SQLiteOpenHelper mDbHelper;

    private static final boolean USE_SHARED_PREFERENCES = false;
    private static final boolean USE_SQLITE = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        myQuestionView = (TextView) this.findViewById(R.id.question_text);
        mLeftButton = (Button) this.findViewById(R.id.left_button);
        myRightButton = (Button) this.findViewById(R.id.right_button);
        mScoreView = (TextView) this.findViewById(R.id.score_view);
        mScoreBase = mScoreView.getText().toString();

        if (savedInstanceState != null) {
            Toast.makeText(SQLMainActivity.this,
                    "create and saved != null",
                    Toast.LENGTH_SHORT).show();
        }
        Context c = getApplicationContext();
        XMLQuizGenerator.createQuizes(c);
        sqlResumeQuiz();
    }

    private void sqlResumeQuiz() {
        mDbHelper = new QuizmasterDbHelper(getApplicationContext());

        // read which incomplete quiz we last took
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // columns we want back
        String[] projection = {
                QuizmasterContract.QuizProgress._ID,
                QuizProgress.COLUMN_NAME_TITLE,
                QuizProgress.COLUMN_NAME_NUMWRONG,
                QuizProgress.COLUMN_NAME_NUMCORRECT,
                QuizProgress.COLUMN_NAME_LASTQUESTION,
                QuizProgress.COLUMN_NAME_TIMESTAMP
        };
        // Filter results WHERE finished = 0
        String selection = QuizProgress.COLUMN_NAME_FINISHED + " = ?";
        String[] selectionArgs = { "0" };
        // sort unfinished quizzes in descending order of timestamp
        // (i.e., most recently updated first)
        String sortOrder = QuizProgress.COLUMN_NAME_TIMESTAMP + " DESC";

        Cursor cursor = db.query(
                QuizProgress.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        String quizTitle = "";
        int numWrong = 0;
        int numCorrect = 0;
        int lastQuestion = 0;

        // we only care about the first row
        if (cursor.moveToNext()) {
            quizTitle = cursor.getString(cursor.getColumnIndexOrThrow(QuizProgress.COLUMN_NAME_TITLE));
            numWrong = cursor.getInt(cursor.getColumnIndexOrThrow(QuizProgress.COLUMN_NAME_NUMWRONG));
            numCorrect = cursor.getInt(cursor.getColumnIndexOrThrow(QuizProgress.COLUMN_NAME_NUMCORRECT));
            lastQuestion = cursor.getInt(cursor.getColumnIndexOrThrow(QuizProgress.COLUMN_NAME_LASTQUESTION));
        }
        cursor.close();

        if (!quizTitle.equals("")) {
            newGame(quizTitle);
        }
    }

    private void sqlAddQuiz() {
        if(mDbHelper == null) {
            mDbHelper = new QuizmasterDbHelper(getApplicationContext());
        }
        // first see if the current quiz already has an entry
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // columns we want back (just id)
        String[] projection = { QuizmasterContract.QuizProgress._ID };
        // Filter results WHERE title is like the current quiz's
        String selection = QuizProgress.COLUMN_NAME_FINISHED + " LIKE ?";
        String[] selectionArgs = { mQuiz.getTitle() };
        // sort quizzes in descending order of timestamp
        // (i.e., most recently updated first)
        String sortOrder = QuizProgress.COLUMN_NAME_TIMESTAMP + " DESC";

        Cursor cursor = db.query(
                QuizProgress.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        // if no rows were returned, insert a new one
        if (!cursor.moveToNext()) {

            // Gets the data repository in write mode
            db = mDbHelper.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(QuizProgress.COLUMN_NAME_TITLE, mQuiz.getTitle());
            values.put(QuizProgress.COLUMN_NAME_FINISHED, 0);
            values.put(QuizProgress.COLUMN_NAME_LASTQUESTION, 0);
            values.put(QuizProgress.COLUMN_NAME_NUMCORRECT, 0);
            values.put(QuizProgress.COLUMN_NAME_NUMWRONG, 0);
            values.put(QuizProgress.COLUMN_NAME_TIMESTAMP, System.currentTimeMillis()/1000L);

            // Insert the new row, returning the primary key value of the new row
            long newRowId = db.insert(QuizProgress.TABLE_NAME, null, values);
        } else {
            // update the row's timestamp
            sqlStoreProgress();
        }
        cursor.close();
    }

    private void sqlStoreProgress() {
        if(mDbHelper == null) {
            mDbHelper = new QuizmasterDbHelper(getApplicationContext());
        }
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(QuizProgress.COLUMN_NAME_TIMESTAMP, System.currentTimeMillis()/1000L);

        // Which row to update, based on the title
        String selection = QuizProgress.COLUMN_NAME_TITLE + " LIKE ?";
        String[] selectionArgs = { mQuiz.getTitle() };

        int count = db.update(
                QuizProgress.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){

        mQuestionIndex = savedInstanceState.getInt(INDEX);
        mCorrect = savedInstanceState.getInt(SCORE);
        askQuestion();
        mQuestionIndex--;  // hack because of state in update
        updateScore();

    }

    @Override
    public void onSaveInstanceState(Bundle state){
        state.putInt(INDEX,mQuestionIndex);
        state.putInt(SCORE,mCorrect);
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    public void leftClick(View button){
        Toast.makeText(SQLMainActivity.this,
                "that's correct! :-)",
                Toast.LENGTH_SHORT).show();
        mCorrect += 1;
        updateScore();
    }

    public void rightClick(View button){

        Toast.makeText(SQLMainActivity.this,
                "that's wrong :-(",
                Toast.LENGTH_SHORT).show();
        updateScore();
    }

    private void updateScore() {
        mQuestionIndex += 1;
        int remaining = mQuiz.size()-mQuestionIndex;
        String s = String.format(" %d/%d with %d to go",mCorrect, mQuiz.size(),remaining);
        mScoreView.setText(mScoreBase+s);
        if (mQuestionIndex < mQuiz.size()){
            askQuestion();
        }
        else {
            Toast.makeText(SQLMainActivity.this,
                    "quiz is over",
                    Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!mMenuInitialized) {
            String[] qtitles = XMLQuizGenerator.getQuizTitles();
            //menu.clear();
            for (String title : qtitles) {
                menu.add(Menu.NONE, Menu.NONE, Menu.NONE, title);
                System.out.println("adding " + title);
            }
            mMenuInitialized = true;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("quizmaster", "MA.onOptionsItemSelected");

        String menuString = (String) item.getTitle();
        newGame(menuString);
        sqlAddQuiz();
        return super.onOptionsItemSelected(item);
    }

    private void newGame(String title) {
        Quiz q = XMLQuizGenerator.getQuiz(title);
        Log.d("quizmaster", "MA.newGame " + title);
        if (q != null) {
            Log.d("quizmaster", "MA.newGame returned " + q.toString());
            mQuiz = q;
            mQuestionIndex = 0;
            mCorrect = 0;
            askQuestion();
        } else {
            Log.d("quizmaster", "MA.newGame returned null");
            newGame();
        }
    }

    private void newGame() {
        mQuiz = XMLQuizGenerator.getQuiz(); //QuizGenerator.getQuiz();
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }
    private void newGame(Quiz q){
        mQuiz = q;
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }

    private void askQuestion() {
        OQuestion q = mQuiz.getQuestion(mQuestionIndex);
        myQuestionView.setText(q.getQuery());
        mLeftButton.setText(q.getCorrectAnswer());
        myRightButton.setText(q.getWrongAnswer());
    }

    public void localCallback(String rawString){
        Log.d("quizmaster", "local callback");
        Toast.makeText(SQLMainActivity.this,
                "fetched "+rawString,
                Toast.LENGTH_LONG).show();


        Quiz q = JSONParser.parse(rawString);
        Log.d("quizmaster","parsed json to quiz for "+q.getTitle());
        newGame(q);


    }

}
