package edu.duke.compsci290.quizmaster;

import java.util.Map;

public class FirestoreQuestion {
    private String mQuery;
    private String mCorrect;
    private Map<String, Boolean> mWrongAnswers;

    public FirestoreQuestion() {
    }

    public String getCorrect() { return mCorrect; }
    public void setCorrect(String c) { mCorrect = c; }
    public String getQuestion() { return mQuery; }
    public void setQuestion(String q) { mQuery = q; }
    public Map<String, Boolean> getIncorrect() { return mWrongAnswers; }
    public void setIncorrect(Map<String, Boolean> wrong) { mWrongAnswers = wrong; }
}
