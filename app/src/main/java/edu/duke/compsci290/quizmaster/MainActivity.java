package edu.duke.compsci290.quizmaster;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.net.MalformedURLException;

public class MainActivity extends AppCompatActivity {

    private Button mLeftButton;
    private Button myRightButton;
    private Quiz mQuiz;
    private TextView myQuestionView;
    private TextView mScoreView;
    private int mQuestionIndex;
    private int mCorrect;
    private String mScoreBase;
    private boolean mMenuInitialized = false;
    private JSONQuizGenerator mJSONQuizGenerator;

    private static String sQUIZTITLE = "quiztitle";
    private static String INDEX = "INDEX";
    private static String SCORE = "SCORE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("QUIZMASTER", "onResume called");

        setContentView(R.layout.activity_main);

        myQuestionView = (TextView) this.findViewById(R.id.question_text);
        mLeftButton = (Button) this.findViewById(R.id.left_button);
        myRightButton = (Button) this.findViewById(R.id.right_button);
        mScoreView = (TextView) this.findViewById(R.id.score_view);
        mScoreBase = mScoreView.getText().toString();

        if (savedInstanceState != null) {
            Toast.makeText(MainActivity.this,
                    "create and saved != null",
                    Toast.LENGTH_SHORT).show();
        }
        Context c = getApplicationContext();
        XMLQuizGenerator.createQuizes(c);
        try {
            mJSONQuizGenerator = new JSONQuizGenerator(R.string.oscars_quiz);//"https://www.cs.duke.edu/csed/quizzes/oscars.json");
        } catch (MalformedURLException e) {
            Log.d("APPMAIN","could not create JSON quiz");
            e.printStackTrace();
        }

        // restore our quiz
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String prevQuiz = sharedPref.getString(sQUIZTITLE, "");
        if (!prevQuiz.equals("")) {
            newGame(prevQuiz);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){

        mQuestionIndex = savedInstanceState.getInt(INDEX);
        mCorrect = savedInstanceState.getInt(SCORE);
        askQuestion();
        mQuestionIndex--;  // hack because of state in update
        updateScore();

    }

    @Override
    public void onSaveInstanceState(Bundle state){
        state.putInt(INDEX,mQuestionIndex);
        state.putInt(SCORE,mCorrect);
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d("QUIZMASTER", "onStart called");
    }

    @Override
    protected void onResume () {
        super.onResume();
        Log.d("QUIZMASTER", "onResume called");
    }

    @Override
    protected void onRestart () {
        super.onRestart();
        Log.d("QUIZMASTER", "onRestart called");
    }

    @Override
    protected void onPause () {
        super.onPause();
        Log.d("QUIZMASTER", "onPause called");
    }

    @Override
    protected void onStop () {
        super.onStop();
        Log.d("QUIZMASTER", "onStop called");
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        Log.d("QUIZMASTER", "onDestroy called");
    }

    public void leftClick(View button){
        Toast.makeText(MainActivity.this,
                "that's correct! :-)",
                Toast.LENGTH_SHORT).show();
        mCorrect += 1;
        updateScore();
    }

    public void rightClick(View button){

        Toast.makeText(MainActivity.this,
                "that's wrong :-(",
                Toast.LENGTH_SHORT).show();
        updateScore();
    }

    private void updateScore() {
        mQuestionIndex += 1;
        int remaining = mQuiz.size()-mQuestionIndex;
        String s = String.format(" %d/%d with %d to go",mCorrect, mQuiz.size(),remaining);
        mScoreView.setText(mScoreBase+s);
        if (mQuestionIndex < mQuiz.size()){
            askQuestion();
        }
        else {
            Toast.makeText(MainActivity.this,
                    "quiz is over",
                    Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!mMenuInitialized) {
            String[] qtitles = XMLQuizGenerator.getQuizTitles();
            //menu.clear();
            for (String title : qtitles) {
                menu.add(Menu.NONE, Menu.NONE, Menu.NONE, title);
            }
            mMenuInitialized = true;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("quizmaster", "MA.onOptionsItemSelected");

        String menuString = (String) item.getTitle();
        if (menuString.equals(getResources().getString(R.string.json_quiz))){
            String json = mJSONQuizGenerator.getJSON(this);
        }
        else {
            newGame(menuString);
        }


            // save which quiz we started taking
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(sQUIZTITLE, mQuiz.getTitle());
            editor.commit();

        return super.onOptionsItemSelected(item);
    }

    private void newGame(String title) {
        Quiz q = XMLQuizGenerator.getQuiz(title);
        Log.d("quizmaster", "MA.newGame " + title);
        if (q != null) {
            Log.d("quizmaster", "MA.newGame returned " + q.toString());
            mQuiz = q;
            mQuestionIndex = 0;
            mCorrect = 0;
            askQuestion();
        } else {
            Log.d("quizmaster", "MA.newGame returned null");
            newGame();
        }
    }

    private void newGame() {
        mQuiz = XMLQuizGenerator.getQuiz(); //QuizGenerator.getQuiz();
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }
    private void newGame(Quiz q){
        mQuiz = q;
        mQuestionIndex = 0;
        mCorrect = 0;
        askQuestion();
    }

    private void askQuestion() {
        OQuestion q = mQuiz.getQuestion(mQuestionIndex);
        myQuestionView.setText(q.getQuery());
        mLeftButton.setText(q.getCorrectAnswer());
        myRightButton.setText(q.getWrongAnswer());
    }

    public void localCallback(String rawString){
        Log.d("quizmaster", "local callback");
        Toast.makeText(MainActivity.this,
                "fetched "+rawString,
                Toast.LENGTH_LONG).show();


        Quiz q = JSONParser.parse(rawString);
        Log.d("quizmaster","parsed json to quiz for "+q.getTitle());
        newGame(q);


    }

}
