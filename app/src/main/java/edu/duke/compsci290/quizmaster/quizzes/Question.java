package edu.duke.compsci290.quizmaster.quizzes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ola on 2/13/18.
 */

public class Question {

    private String mQuestion;
    private List<IAnswer> mAnswers;
    private Set<IAnswer> mCorrect;
    private Set<IAnswer> mChosen;

    Question(String source){
        mQuestion = source;
        mAnswers = new ArrayList<>();
        mCorrect = new HashSet<>();
        mChosen = new HashSet<>();
    }
    // add an answer to this question
    void addAnswer(IAnswer answer){
        mAnswers.add(answer);
    }

    // add a correct answer to this question
    void addCorrectAnswer(CorrectAnswer answer) { mCorrect.add(answer); }

    // returns the string question
    public String getQuestion(){
        return mQuestion;
    }

    // iterate through all answers and correct answers
    public Iterable<IAnswer> answerIterable(){
        return mAnswers;
    }
    public Iterable<IAnswer> correctAnswerIterable() {return mCorrect; }

    // add a chosen answer and iterate through the chosen answers
    public void processChosen(IAnswer answer){ mChosen.add(answer); }
    public Iterable<IAnswer> chosenIterable(){ return mChosen; }

}
