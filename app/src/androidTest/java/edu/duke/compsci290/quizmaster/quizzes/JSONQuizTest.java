package edu.duke.compsci290.quizmaster.quizzes;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import edu.duke.compsci290.quizmaster.JSONParser;
import edu.duke.compsci290.quizmaster.JSONQuizGenerator;
import edu.duke.compsci290.quizmaster.OQuestion;
import edu.duke.compsci290.quizmaster.Quiz;
import edu.duke.compsci290.quizmaster.R;

import static org.junit.Assert.assertEquals;

/**
 * Created by lpcox on 2/26/18.
 */

@RunWith(AndroidJUnit4.class)
public class JSONQuizTest {
    private static final String OSCAR_QUIZ_TILE = "Best picture winners";
    private static final int OSCAR_QUIZ_NUM_QUESTIONS = 3;
    private static final OQuestion[] OSCAR_QUIZ_QUESTIONS =
            new OQuestion[]{
                    new OQuestion("Best picture in 2015", "Spotlight",
                            new String[] {"The Big Short",
                                    "Bridge of Spies",
                                    "Brooklyn",
                                    "Mad Max: Fury Road",
                                    "The Martian",
                                    "The Revenant",
                                    "Room"}
                    ),
                    new OQuestion("Best picture in 2014", "Birdman",
                            new String[] {"American Sniper",
                                    "Boyhood",
                                    "The Grand Budapest Hotel",
                                    "The Imitation Game",
                                    "Selma",
                                    "The Theory of Everything",
                                    "Whiplash"}
                    ),
                    new OQuestion("Best picture in 2013", "12 Years a Slave",
                            new String[] {"American Hustle",
                                    "Captain Phillips",
                                    "Dallas Buyers Club",
                                    "Gravity",
                                    "Her",
                                    "Nebraska",
                                    "Philomena",
                                    "The Wolf of Wall Street"}
                    )
            };

    @Test
    public void quizFromRIsCorrect() throws Exception {
        // Context of the app under test.
        Context c = InstrumentationRegistry.getTargetContext();
        JSONQuizGenerator generator = new JSONQuizGenerator(R.string.oscars_quiz);
        Quiz quiz = JSONParser.parse(generator.getJSON(c));
        // make sure the title matches
        String title = quiz.getTitle();
        assertEquals(OSCAR_QUIZ_TILE, title);

        // make sure the number of questions is right
        int numQuestions = quiz.size();
        assertEquals(OSCAR_QUIZ_NUM_QUESTIONS, numQuestions);

        // make sure the questions are right
        for (int i=0; i<numQuestions; i++) {
            OQuestion q = quiz.getQuestion(i);
            assertEquals(OSCAR_QUIZ_QUESTIONS[i], q);
        }
    }

    @Test
    public void quizFromJSONIsCorrect() {
        String title = "Game Recap";
        String query = "Did Duke win last night?";
        String correctAnswer = "no";
        String incorrectAnswer = "yes";
        int numQuestions = 1;

        String jsonTitle = "\"title\":" + "\"" + title + "\"";
        String jsonQuestions =
                "\"questions\":[{" + "\"question\":\"" + query + "\"," +
                        "\"correct\":\"" + correctAnswer + "\"," +
                        "\"incorrect\":[\"" + incorrectAnswer +
                        "\"]" +
                        "}]";

        String json = "{" + jsonTitle + "," + jsonQuestions + "}";
        Quiz quiz = JSONParser.parse(json);

        // make sure the title matches
        assertEquals(title, quiz.getTitle());

        // make sure the number of questions is right
        assertEquals(numQuestions, quiz.size());

        // make sure the questions are right
        OQuestion q = quiz.getQuestion(0);
        OQuestion myQ = new OQuestion(query, correctAnswer, incorrectAnswer);
//        assertEquals(myQ, q);
    }

    static {
    }
}
